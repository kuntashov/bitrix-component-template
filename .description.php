<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

$arComponentDescription = array(
    "ID"            => "mh-component-template",
    "NAME"          => "Шаблон компонента",
    "DESCRIPTION"   => "Шаблон простого компонента для 1С-Битрикс",
    "ICON"          => "",
    "COMPLEX"       => "N",
    "PATH" => array(
        "ID"    => "mediahero",
        "NAME"  => "Медиагерой",
    ),
);

?>